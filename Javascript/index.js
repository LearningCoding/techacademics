let navbar = document.getElementById('menubar');
window.onscroll = function() {
    let navbarY = window.pageYOffset;
    console.log(navbarY);
    if (navbarY > 5) {
        navbar.style.background = 'linear-gradient(#5c82ab, #71bdaf)';
    } else {
        navbar.style.background = 'rgba(0, 0, 0, 0.2)';
    }
}